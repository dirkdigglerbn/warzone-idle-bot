# Warzone Idle bot
This program is designed for farming XP and battle pass progression on Modern Warfare (2019) 
using Warzone's Battle Royale gamemode by continuously queuing for matches and moving around 
in game to avoid kicks due to inactivity.  
The bot scans for different UI elements & prompts throughout the game to navigate menus.

## Setup
* Install Python 3 from https://www.python.org/downloads/ (preferably one of the latest versions). Check the option
"Add to PATH" to be able to launch python from the commandline.
* Install the packages required by the program using PIP with the command
"python -m pip install ... (or "python3 -m pip install ..."). The required packages are:
    * pypiwin32 : for access to the Windows API (windowing, input, ...).
    * pillow : used to take screenshots of the desktop.
    * opencv-python : used for image analysis.
* Download this program.
* Set Modern Warfare's language to English, as this is the UI language scanned for by this bot.
    * If you want the watchdog feature (that restarts the game in case of freezes or crashes),
    you also need to set the Battle.net launcher's language to english, and keep the window opened.
* Set Modern Warfare to run in Windowed mode, at a resolution of 1280x720. The bot will resize the window automatically
if the wrong resolution is set.
* Make sure that the "jump" button is bound to "SPACE" so that the idler can drop from the ship.

## How to run
Simply start the game, and once it is up, run the program using the command 
"python bot.py" (or "python3 bot.py")
(You may also start it by double cliking the file if it is set as 'open with python').
The bot should start its keypresses 5 seconds after launch.  
You can specify to the bot which gamemode (Battle royale, Plunder, Blood Money, ...) you wish to queue for.
To specify the gamemode, simply provide the name of the gamemode as an argument to the 'mode' option
(eg "python bot.py mode=br-solos" to queue for the Battle royale Solos gamemode).
The different gamemodes that are supported are:
* "br-quads" for the Battle Royale Quads gamemode.
* "br-trios" for the Battle Royale Trios gamemode.
* "br-duos" for the Battle Royale Duos gamemode.
* "br-solos" for the Battle Royale Solos gamemode.
* "vanguard-br-quads" for the Vanguard Royale Quads gamemode.
* "vanguard-resurgence-quads" for the Vanguard Resurgence Quads gamemode.
* "rebirth-resurgence-duos" for the Rebirth Resurgence Duos gamemode.
* "plunder-trios" for the Plunder Trios gamemode.

You can also specify whether to fill your lobby or not with the 'fill' option that can be set to either 'on' or 'off'

Note that in Plunder, Plunder Blood Money and Kings Slayer modes, it is necessary to pick a loadout.
The bot will look for a loadout named "My loadout" (See image below),
therefore if you wish to queue for these gamemodes, name one of your loadouts "My loadout".  
![My-loadout](./Docs/my_loadout_example.png)

The bot defaults to the Battle Royale Quads gamemode.
