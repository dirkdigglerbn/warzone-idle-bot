import keyinput, time, win32gui, win32api, win32con, win32com.client, win32process, cv2, sys, ctypes
from PIL import ImageGrab
import numpy as np
import os, signal, threading
import re

# The directory of this script
pwd = os.path.dirname(os.path.realpath(sys.argv[0])) + "\\"


def path(relpath: str) -> str:
    return pwd + relpath


# The template inside the name of the window we are looking for
# Yes, this clusterfuck is the standard name 'Call of Duty®: Modern Warfare®',
# With lots of Zero Width Spaces (U+200B) inserted inbetween, big Infinity-Ward anti-cheat !
window_template = '\u0043\u200b\u0061\u200b\u006c\u200b\u006c\u200b\u0020\u200b\u006f\u200b\u0066\u200b\u0020\u200b' \
                  '\u0044\u200b\u0075\u200b\u0074\u200b\u0079\u200b\u00ae\u200b\u003a\u200b\u0020\u200b\u004d\u200b' \
                  '\u006f\u200b\u0064\u200b\u0065\u200b\u0072\u200b\u006e\u200b\u0020\u200b\u0057\u200b\u0061\u200b' \
                  '\u0072\u200b\u0066\u200b\u0061\u200b\u0072\u200b\u0065\u200b\u00ae'

# The name of the "Start in safe mode" window,
# in english (feel free to modify into your mother tongue)
safe_mode_window_name = 'Run In Safe Mode?'

# The name of the "DEV ERROR" windows (usually related to DirectX errors),
# in english (feel free to modify into your mother tongue)
dev_error_window_name = 'Fatal Error'

# The name of the battle.net window
battle_net_window_name = 'Battle.net'

# The complete name of the window that contains this template
window_name = None

# Position of the modern warfare window
window_x = -1
window_y = -1
window_width = -1
window_height = -1

# Windows process related stuff
window_pid = -1
window_path = ""
window_found = False

# Path to the game's directory
game_directory_path = ""

# Launch options parsed as arguments in the "optionname" or "optioname=value" format
launch_options = {}

# Routine to kill a process, used when it gets frozen permanently
def killprocess(pid):
    os.kill(pid, signal.SIGTERM)


# Callback for windows enumeration
def getmwwindow(hwnd, extra):
    w_name = win32gui.GetWindowText(hwnd)

    if window_template not in w_name:
        return

    global window_name
    window_name = w_name

    global window_found
    window_found = True

    global window_pid
    global window_path
    global game_directory_path
    thread_id, window_pid = win32process.GetWindowThreadProcessId(hwnd)

    handle = win32api.OpenProcess(win32con.PROCESS_QUERY_INFORMATION | win32con.PROCESS_VM_READ, False, window_pid)
    window_path = win32process.GetModuleFileNameEx(handle, 0)
    game_directory_path = os.path.dirname(window_path)

    rect = win32gui.GetClientRect(hwnd)
    rect_with_borders = win32gui.GetWindowRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))
    global window_x
    global window_y
    global window_width
    global window_height
    window_x = window_pos[0]
    window_y = window_pos[1]
    window_width = rect[2]
    window_height = rect[3]

    # Get the size of borders
    window_width_with_borders = rect_with_borders[2] - rect_with_borders[0]
    window_height_with_borders = rect_with_borders[3] - rect_with_borders[1]

    total_border_width = window_width_with_borders - window_width
    total_border_height = window_height_with_borders - window_height
    # Horizontally, the border only comprises of the snapping border, which wraps the sides (and also the bottom)
    snapping_border_width = total_border_width / 2
    # But not the top of the top bar !
    top_bar_height = total_border_height - snapping_border_width

    # Check for whether or not the window has the right resolution
    if window_width != 1280 or window_height != 720:
        # If not, resize the window. Take into account the fact we are mixing coordinates with no borders
        # and coordinates with borders
        win32gui.SetWindowPos(hwnd,
                              0,
                              int(window_x - snapping_border_width),
                              int(window_y - top_bar_height),
                              int(1280 + total_border_width),
                              int(720 + total_border_height),
                              0)
        window_width = 1280
        window_width = 720


# Keys to press to move around
keys = [ keyinput.W, keyinput.D, keyinput.S, keyinput.A ]


# Function to move around ingame
def move_around():
    for key in keys:
        keyinput.holdKey(key, 1.0)


# Function to press space
def press_space_delayed(x, y):
    # Wait for 3 seconds
    time.sleep(3.0)
    keyinput.pressKey(keyinput.SPACE)
    time.sleep(0.05)
    keyinput.releaseKey(keyinput.SPACE)


# Function to delay further the click after the cursor has been moved,
# in case we can't rely on a hovered version of a button
def click_delayed_3_seconds(x, y):
    keyinput.click(x, y, 3.0)


# Functions to switch to fill or no-fill
def switch_to_no_fill(x, y):
    keyinput.click(x + 107, y)


def switch_to_fill(x, y):
    keyinput.click(x + 91, y)


# Find a certain item in an image, returns the estimated position and associated threshold
def findItem(img, template):
    # Apply template Matching
    res = cv2.matchTemplate(img, template, cv2.TM_CCOEFF_NORMED)

    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    w, h = template.shape[::-1]

    # Top left of the area
    top_left = max_loc
    # Buttom right of the area
    bottom_right = (top_left[0] + w, top_left[1] + h)

    # Zone to click if we do need
    middle_x = (top_left[0] + bottom_right[0]) / 2
    middle_y = (top_left[1] + bottom_right[1]) / 2

    return {'threshold' : max_val, 'x' : middle_x, 'y' : middle_y}


# Take a screenshot of the window
def screenshot(x, y, width, height):
    image = ImageGrab.grab(bbox = (x, y, x + width, y + height))
    image = cv2.cvtColor(np.array(image), cv2.COLOR_RGB2BGR)
    return image

# The previous image used in the watchdog
old_img = None

# The play button in the battle net window
battle_net_play_button = cv2.imread(path("Images/Extras/battle_net_play_button.png"), cv2.IMREAD_GRAYSCALE)
battle_net_play_button_hovered = cv2.imread(path("Images/Extras/battle_net_play_button_hovered.png"), cv2.IMREAD_GRAYSCALE)


# Check for the play button in the battle net window
def checkForBattleNetPlayButton() -> bool:
    # Try to find the window handle
    hwnd = win32gui.FindWindow(None, battle_net_window_name)

    if hwnd == 0:
        return False

    rect = win32gui.GetClientRect(hwnd)
    window_pos = win32gui.ClientToScreen(hwnd, (0, 0))

    # Put it in foreground in case it is not and wait for a bit to ensure it is made visible
    # Workaround for foreground windows
    shell = win32com.client.Dispatch("WScript.Shell")
    shell.SendKeys('%')

    try:
        win32gui.SetForegroundWindow(hwnd)
    except:
        print('Error: SetForegroundWindow failed')

    time.sleep(1.0)

    # Screenshot the safe mode window
    battle_net_window_img = screenshot(window_pos[0], window_pos[1], rect[2], rect[3])

    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(battle_net_window_img, cv2.COLOR_BGR2GRAY)

    # Try to find the "Play" button
    result = findItem(img, battle_net_play_button)

    # Click on it if was found
    if result['threshold'] > 0.90:
        keyinput.click(window_pos[0] + int(result['x']), window_pos[1] + int(result['y']))
        return True

    # Try to find the "Play" button if it is hovered
    result = findItem(img, battle_net_play_button_hovered)

    # Click on it if was found
    if result['threshold'] > 0.90:
        keyinput.click(window_pos[0] + int(result['x']), window_pos[1] + int(result['y']))
        return True

    return False

# Watchdog running on another thread, makes sure the Apex process didn't crash or is not stucked
def processwatchdog():
    while True:
        # Check for the process being alive every 60 seconds
        time.sleep(60)

        global window_pid
        global window_path
        global window_found

        hwnd = 0

        # Try to find the window handle
        if window_name is not None:
            hwnd = win32gui.FindWindow(None, window_name)

        # Make sure the Window is still alive
        if hwnd == 0:
            window_found = False

            # Delete the 'lock file' that the game creates when it's active,
            # to make sure the safemode window doesn't pop up
            mw_lock_file = game_directory_path + "\\__ModernWarfare"

            try:
                os.remove(mw_lock_file)
            except OSError: # File may not exist anymore
                pass

            # Check for the battle net window
            if checkForBattleNetPlayButton() is True:
                continue

            # In case the window name changed slightly (P100 Infinity Ward anti cheat technique), try to re-fetch it
            win32gui.EnumWindows(getmwwindow, None)

            # If it is still not found, wait again
            if window_found is False:
                continue

        # Refresh the window's position
        win32gui.EnumWindows(getmwwindow, None)

        # Grab its handle
        hwnd = win32gui.FindWindow(None, window_name)

        # Put it in foreground
        # Workaround for foreground windows
        shell = win32com.client.Dispatch("WScript.Shell")
        shell.SendKeys('%')
        win32gui.SetForegroundWindow(hwnd)

        global old_img

        # Capture the window in color
        color_img = screenshot(window_x, window_y, window_width, window_height)

        # Convert it to grayscale for faster processing
        img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

        if (old_img is not None):
            # Compare the current image to the old one,
            # if it didn't change within 60 seconds, the process is likely stuck
            result = findItem(img, old_img)

            # We deem the image as being the same if it matches over 99.999%
            if result['threshold'] > 0.99999:
                # Kill the process if it matches
                killprocess(window_pid)

        # Save the current image to compare it to the next one being read
        old_img = img


# Templates that we are looking for in the image
warzone_icon1 = cv2.imread(path("Images/warzone_icon1.png"), cv2.IMREAD_GRAYSCALE)
warzone_icon2 = cv2.imread(path("Images/warzone_icon2.png"), cv2.IMREAD_GRAYSCALE)
warzone_icon3 = cv2.imread(path("Images/warzone_icon3.png"), cv2.IMREAD_GRAYSCALE)
warzone_play_button = cv2.imread(path("Images/warzone_play_button.png"), cv2.IMREAD_GRAYSCALE)
play_button = cv2.imread(path("Images/play_button.png"), cv2.IMREAD_GRAYSCALE)
play_button2 = cv2.imread(path("Images/play_button2.png"), cv2.IMREAD_GRAYSCALE)
space_prompt = cv2.imread(path("Images/space_prompt.png"), cv2.IMREAD_GRAYSCALE)
leave_game_confirm_yes_button = cv2.imread(path("Images/leave_game_confirm_yes_button.png"), cv2.IMREAD_GRAYSCALE)
leave_game_confirm_yes_button_hovered = cv2.imread(path("Images/leave_game_confirm_yes_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
leave_game_with_party_button = cv2.imread(path("Images/leave_game_with_party_button.png"), cv2.IMREAD_GRAYSCALE)
leave_game_with_party_button_hovered = cv2.imread(path("Images/leave_game_with_party_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
leave_with_party_button = cv2.imread(path("Images/leave_with_party_button.png"), cv2.IMREAD_GRAYSCALE)
leave_with_party_button_hovered = cv2.imread(path("Images/leave_with_party_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
announcement_close_button = cv2.imread(path("Images/announcement_close_button.png"), cv2.IMREAD_GRAYSCALE)
announcement_close_button_hovered = cv2.imread(path("Images/announcement_close_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
continue_button = cv2.imread(path("Images/continue_button.png"), cv2.IMREAD_GRAYSCALE)
continue_button_hovered = cv2.imread(path("Images/continue_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
exit_button = cv2.imread(path("Images/exit_button.png"), cv2.IMREAD_GRAYSCALE)
exit_button_hovered = cv2.imread(path("Images/exit_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
leave_game_button = cv2.imread(path("Images/leave_game_button.png"), cv2.IMREAD_GRAYSCALE)
leave_game_button_hovered = cv2.imread(path("Images/leave_game_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
back_button = cv2.imread(path("Images/back_button.png"), cv2.IMREAD_GRAYSCALE)
back_button_hovered = cv2.imread(path("Images/back_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
quit_to_desktop_button = cv2.imread(path("Images/quit_to_desktop_button.png"), cv2.IMREAD_GRAYSCALE)
quit_to_desktop_button_hovered = cv2.imread(path("Images/quit_to_desktop_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
battle_pass_update_no_thanks_button = cv2.imread(path("Images/battle_pass_update_no_thanks_button.png"), cv2.IMREAD_GRAYSCALE)
battle_pass_update_no_thanks_button_hovered = cv2.imread(path("Images/battle_pass_update_no_thanks_button_hovered.png"), cv2.IMREAD_GRAYSCALE)

# Additional templates that may be added depending on what the user selects

# When queuing for modes where you have to select a loadout (e.g. Plunder)
my_loadout_button = cv2.imread(path("Images/my_loadout_button.png"), cv2.IMREAD_GRAYSCALE)
my_loadout_button_hovered = cv2.imread(path("Images/my_loadout_button_hovered.png"), cv2.IMREAD_GRAYSCALE)

# When queuing one of the "old school 150 players" battle royale modes
battle_royale_button = cv2.imread(path("Images/battle_royale_button.png"), cv2.IMREAD_GRAYSCALE)

# When queuing the battle royale quads mode
battle_royale_quads_button = cv2.imread(path("Images/battle_royale_quads_button.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_quads_button_hovered = cv2.imread(path("Images/battle_royale_quads_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_quads_ui_elements = [{"image" : battle_royale_quads_button, "threshold" : 0.80, "callback" : keyinput.click},
                                   {"image" : battle_royale_quads_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                                   {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the battle royale trios mode
battle_royale_trios_button = cv2.imread(path("Images/battle_royale_trios_button.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_trios_button_hovered = cv2.imread(path("Images/battle_royale_trios_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_trios_ui_elements = [{"image" : battle_royale_trios_button, "threshold" : 0.80, "callback" : keyinput.click},
                                   {"image" : battle_royale_trios_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                                   {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the battle royale duos mode
battle_royale_duos_button = cv2.imread(path("Images/battle_royale_duos_button.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_duos_button_hovered = cv2.imread(path("Images/battle_royale_duos_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_duos_ui_elements = [{"image" : battle_royale_duos_button, "threshold" : 0.80, "callback" : keyinput.click},
                                  {"image" : battle_royale_duos_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                                  {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the battle royale solos mode
battle_royale_solos_button = cv2.imread(path("Images/battle_royale_solos_button.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_solos_button_hovered = cv2.imread(path("Images/battle_royale_solos_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
battle_royale_solos_ui_elements = [{"image" : battle_royale_solos_button, "threshold" : 0.80, "callback" : keyinput.click},
                                   {"image" : battle_royale_solos_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                                   {"image" : battle_royale_button, "threshold" : 0.80, "callback" : keyinput.move}]

# When queuing for the vanguard royale quads mode
vanguard_royale_quads_button = cv2.imread(path("Images/vanguard_royale_quads_button.png"), cv2.IMREAD_GRAYSCALE)
vanguard_royale_quads_button_hovered = cv2.imread(path("Images/vanguard_royale_quads_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
vanguard_royale_quads_ui_elements = [{"image" : vanguard_royale_quads_button, "threshold" : 0.80, "callback" : keyinput.click},
                                     {"image" : vanguard_royale_quads_button_hovered, "threshold" : 0.85, "callback" : keyinput.click}]

# When queuing for the vanguard resurgence quads mode
vanguard_resurgence_quads_button = cv2.imread(path("Images/vanguard_resurgence_quads_button.png"), cv2.IMREAD_GRAYSCALE)
vanguard_resurgence_quads_button_hovered = cv2.imread(path("Images/vanguard_resurgence_quads_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
vanguard_resurgence_quads_ui_elements = [{"image" : vanguard_resurgence_quads_button, "threshold" : 0.80, "callback" : keyinput.click},
                                         {"image" : vanguard_resurgence_quads_button_hovered, "threshold" : 0.85, "callback" : keyinput.click}]

# When queuing for the rebirth resurgence duos mode
rebirth_resurgence_duos_button = cv2.imread(path("Images/rebirth_resurgence_duos_button.png"), cv2.IMREAD_GRAYSCALE)
rebirth_resurgence_duos_button_hovered = cv2.imread(path("Images/rebirth_resurgence_duos_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
rebirth_resurgence_duos_ui_elements = [{"image" : rebirth_resurgence_duos_button, "threshold" : 0.80, "callback" : keyinput.click},
                                       {"image" : rebirth_resurgence_duos_button_hovered, "threshold" : 0.85, "callback" : keyinput.click}]

# When queuing for the plunder trios mode
plunder_trios_button = cv2.imread(path("Images/plunder_trios_button.png"), cv2.IMREAD_GRAYSCALE)
plunder_trios_button_hovered = cv2.imread(path("Images/plunder_trios_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
plunder_trios_ui_elements = [{"image" : plunder_trios_button, "threshold" : 0.80, "callback" : keyinput.click},
                             {"image" : plunder_trios_button_hovered, "threshold" : 0.85, "callback" : keyinput.click},
                             {"image": my_loadout_button, "threshold": 0.80, "callback": keyinput.click},
                             {"image": my_loadout_button_hovered, "threshold": 0.85, "callback": keyinput.click}]

optional_ui_elements = {"br-quads" : battle_royale_quads_ui_elements,
                        "br-trios" : battle_royale_trios_ui_elements,
                        "br-duos" : battle_royale_duos_ui_elements,
                        "br-solos" : battle_royale_solos_ui_elements,
                        "vanguard-resurgence-quads" : vanguard_resurgence_quads_ui_elements,
                        "rebirth-resurgence-duos" : rebirth_resurgence_duos_ui_elements,
                        "plunder-trios" : plunder_trios_ui_elements,
                        "vanguard-br-quads" : vanguard_royale_quads_ui_elements}

# Fill & no fill
dont_fill_button = cv2.imread(path("Images/dont_fill_button.png"), cv2.IMREAD_GRAYSCALE)
dont_fill_button_hovered = cv2.imread(path("Images/dont_fill_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
fill_ui_elements = [{"image" : dont_fill_button, "threshold" : 0.85, "callback" : switch_to_fill},
                    {"image" : dont_fill_button_hovered, "threshold" : 0.90, "callback" : switch_to_fill}]

fill_button = cv2.imread(path("Images/fill_button.png"), cv2.IMREAD_GRAYSCALE)
fill_button_hovered = cv2.imread(path("Images/fill_button_hovered.png"), cv2.IMREAD_GRAYSCALE)
dont_fill_ui_elements = [{"image" : fill_button, "threshold" : 0.85, "callback" : switch_to_no_fill},
                         {"image" : fill_button_hovered, "threshold" : 0.90, "callback" : switch_to_no_fill}]


ui_elements = [{"image" : warzone_play_button, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : exit_button, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : exit_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : warzone_icon1, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : warzone_icon2, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : warzone_icon3, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : play_button, "threshold" : 0.95, "callback" : click_delayed_3_seconds},
               {"image" : play_button2, "threshold" : 0.95, "callback" : click_delayed_3_seconds},
               {"image" : space_prompt, "threshold" : 0.90, "callback" : press_space_delayed},
               {"image" : leave_game_confirm_yes_button, "threshold" : 0.75, "callback" : keyinput.click},
               {"image" : leave_game_confirm_yes_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : leave_game_with_party_button, "threshold" : 0.75, "callback" : keyinput.click},
               {"image" : leave_game_with_party_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : leave_with_party_button, "threshold" : 0.75, "callback" : keyinput.click},
               {"image" : leave_with_party_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : announcement_close_button, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : announcement_close_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : continue_button, "threshold" : 0.85, "callback" : keyinput.click},
               {"image" : continue_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : leave_game_button, "threshold" : 0.75, "callback" : keyinput.click},
               {"image" : leave_game_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : back_button, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : back_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : quit_to_desktop_button, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : quit_to_desktop_button_hovered, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : battle_pass_update_no_thanks_button, "threshold" : 0.95, "callback" : keyinput.click},
               {"image" : battle_pass_update_no_thanks_button_hovered, "threshold" : 0.95, "callback" : keyinput.click}]


available_launch_options = [{'name' : 'mode=<mode_name>', 'description' : 'gamemode to queue for'},
                            {'name' : 'fill=on/off', 'description' : 'whether or not to fill the lobby with random players'}]


# Parse launch options if any
if len(sys.argv) > 1:
    # Regex used to parse options
    arg_regex_template = '(.*)=(.*)'
    arg_regex = re.compile(arg_regex_template)

    for argv in sys.argv[1:]:
        match = arg_regex.match(argv)
        if match is not None:
            launch_options[match.group(1)] = match.group(2)
        else:
            launch_options[argv] = None


if 'mode' in launch_options:
    mode = launch_options.pop('mode', None)
    if mode in optional_ui_elements:
        print(f"Selected gamemode: {mode}")
        for ui_element in optional_ui_elements[mode]:
            ui_elements.append(ui_element)
    else:
        print(f"Unknown gamemode: '{mode}', exiting.")
        sys.exit(1)
else:
    print("No gamemode specified, defaulting to 'br-quads'")
    for ui_element in optional_ui_elements["br-quads"]:
        ui_elements.append(ui_element)


if 'fill' in launch_options:
    fill = launch_options.pop('fill', None)
    # As this needs to be done before selecting a gamemode, prepend rather than append
    if fill == 'on':
        for ui_element in fill_ui_elements:
            ui_elements.insert(0, ui_element)
    elif fill == 'off':
        for ui_element in dont_fill_ui_elements:
            ui_elements.insert(0, ui_element)
    else:
        print("Unknown value for 'fill' option, can either be 'on' or 'off'")
        sys.exit(1)


# Print leftover launch options as unknown
for unknown_launch_option in launch_options:
    print(f"Unknown launch option: '{unknown_launch_option}'")

if launch_options:
    print("List of options:")
    for available_launch_option in available_launch_options:
        print(f"- {available_launch_option['name']} : {available_launch_option['description']}")
    sys.exit(1)


# Set ourselves as DPI aware, or else we won't get proper pixel coordinates if scaling is not 100%
errorCode = ctypes.windll.shcore.SetProcessDpiAwareness(2)

print("Modern Warfare bot starting in 5 seconds, bring Modern Warfare window in focus...")
time.sleep(5)

win32gui.EnumWindows(getmwwindow, None)

# If the window hasn't been found, exit
if window_found == False:
    print("No Modern Warfare window found")
    sys.exit(1)

# Start the watchdog thread
watchdogthread = threading.Thread(target = processwatchdog)
watchdogthread.start()

while True:
    # If the window died, stop running analysis for a moment */
    if window_found is False:
        time.sleep(5)
        continue

    # Capture the window in color
    color_img = screenshot(window_x, window_y, window_width, window_height)
    # Convert it to grayscale for faster processing
    img = cv2.cvtColor(color_img, cv2.COLOR_BGR2GRAY)

    found_UI_element = False

    # Try to find any of the templates
    for ui_element in ui_elements:
        result = findItem(img, ui_element['image'])

        if result['threshold'] > ui_element['threshold']:
            found_UI_element = True
            ui_element['callback'](window_x + int(result['x']), window_y + int(result['y']))
            break

    # If no UI element got found, move around is the default behavior
    if found_UI_element is False:
        move_around()

    # Sleep for a second
    time.sleep(1.0)
